package br.com.gtcom.ccee.api.controller;

import br.com.gtcom.ccee.api.core.handler.ApiErrorResponse;
import br.com.gtcom.ccee.api.service.IUploadService;
import br.com.gtcom.ccee.api.vo.upload.UploadResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Log4j2
@Validated
@CrossOrigin(origins = "*")
@RestController
@RequestMapping(path = "/api/v1.0/upload", produces = {MediaType.APPLICATION_JSON_VALUE})
public class UploadController {

    @Autowired
    private IUploadService uploadService;

    @Operation(summary = "Realizar upload de arquivos XML", responses = {
            @ApiResponse(description = "Successful Operation", responseCode = "200", content = @Content(mediaType = "application/json", schema = @Schema(implementation = UploadResponse.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class))),
            @ApiResponse(responseCode = "401", description = "Unauthorized", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "403", description = "Access Denied", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Not found", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "500", description = "Internal Server Erro", content = @Content(mediaType = "application/json", schema = @Schema(implementation = ApiErrorResponse.class)))
    })
    @PostMapping("files")
    public UploadResponse files(@Parameter(description = "Informar o(s) arquivo(s) para realizar o upload")
                                @RequestParam final MultipartFile[] files
    ) {
        log.debug("BEGIN files: totalFiles={}", files.length);

        var response = uploadService.files(files);

        log.debug("END files: response={}", response);

        return response;
    }
}