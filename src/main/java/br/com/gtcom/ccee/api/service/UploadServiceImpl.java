package br.com.gtcom.ccee.api.service;

import br.com.gtcom.ccee.api.core.exception.CustomException;
import br.com.gtcom.ccee.api.vo.upload.UploadResponse;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.Arrays;

@Log4j2
@Service
public class UploadServiceImpl implements IUploadService {

	public UploadResponse files(final MultipartFile[] files){
		try {
			Arrays.asList(files).stream().forEach(file -> processFile(file));

			return UploadResponse.builder().success(true).message("Upload realizado com sucesso!").build();
		} catch (Exception ex){
			return UploadResponse.builder().success(false).message(ex.getMessage()).build();
		}
	}

	private void processFile(MultipartFile file) {
		log.debug("BEGIN processFile: filename={}", file.getOriginalFilename());

		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.parse(file.getInputStream());
			document.getDocumentElement().normalize();
			NodeList nList = document.getElementsByTagName("agente");

			for (int i = 0; i < nList.getLength(); i++) {
				Node nNode = nList.item(i);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					var codigoAgente = eElement.getElementsByTagName("codigo").item(0).getTextContent();
					log.debug("CodigoAgente={}", codigoAgente);
					System.out.println("Código Agente: " + codigoAgente);
				}
			}

		} catch (Exception ex) {
			throw new CustomException("Erro ao processar o arquivo " + file.getOriginalFilename(), ex);
		}

		log.debug("END processFile: filename={}", file.getOriginalFilename());
	}

}
