package br.com.gtcom.ccee.api.core.swagger;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Value("${info.app.version}")
    private String appVersion;

    @Value("${info.app.name}")
    private String appName;

    @Value("${info.app.description}")
    private String appDescription;

    @Bean
    public OpenAPI configOpenAPIDefinition() {
        final String securitySchemeName = "Bearer";
        final var securityDescription = new StringBuilder();
        securityDescription.append("<p> JWT Authorization header using the Bearer scheme. </p>");
        securityDescription.append("<p> Enter 'Bearer' [space] and then your token in the text input below. </p>");
        securityDescription.append("<p> <b>Example:</b> 'Bearer 1232abcbdbdnd' </p>");
        securityDescription.append("<p> <b>Name:</b> Authorization </p>");
        securityDescription.append("<p> <b>In:</b> header </p>");

        return new OpenAPI()
                .addSecurityItem(new SecurityRequirement().addList(securitySchemeName))
                .components(new Components().addSecuritySchemes(securitySchemeName,
                        new SecurityScheme()
                                .name(securitySchemeName)
                                .description(securityDescription.toString())
                                .type(SecurityScheme.Type.HTTP)
                                .scheme("bearer")
                                .bearerFormat("JWT")))
                .info(new Info().title(appName).description(appDescription).version(appVersion));
    }

    @Bean
    public GroupedOpenApi configApiV1OpenApi() {
        return GroupedOpenApi.builder().group("v1.0").packagesToScan("br.com.gtcom.ccee.api.controller").pathsToMatch("/api/v1.0/**").build();
    }

}
