package br.com.gtcom.ccee.api.service;

import br.com.gtcom.ccee.api.vo.upload.UploadResponse;
import org.springframework.web.multipart.MultipartFile;

public interface IUploadService {

	public UploadResponse files(final MultipartFile[] files);
}
