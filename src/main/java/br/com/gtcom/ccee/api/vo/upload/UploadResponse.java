package br.com.gtcom.ccee.api.vo.upload;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class UploadResponse {

	private boolean success;

	private String message;

}
